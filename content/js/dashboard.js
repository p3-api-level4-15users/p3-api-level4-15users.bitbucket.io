/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 93.73776908023483, "KoPercent": 6.262230919765166};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.2583170254403131, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.5614035087719298, 500, 1500, "getChildHealthRecords"], "isController": false}, {"data": [0.8545454545454545, 500, 1500, "getClassAttendanceSummaries"], "isController": false}, {"data": [0.125, 500, 1500, "childByID"], "isController": false}, {"data": [0.0, 500, 1500, "getChildStatementOfAccount"], "isController": false}, {"data": [0.0, 500, 1500, "findClassActivities"], "isController": false}, {"data": [0.06896551724137931, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.0, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [0.7192982456140351, 500, 1500, "bankAccountsByFkChild"], "isController": false}, {"data": [0.01818181818181818, 500, 1500, "getChildrenData"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 511, 32, 6.262230919765166, 8168.383561643834, 4, 51364, 4044.0, 21928.600000000002, 29686.6, 38119.159999999996, 1.6689474526505563, 2.3840734983963734, 5.889180822577169], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getChildHealthRecords", 57, 4, 7.017543859649122, 1334.4035087719299, 4, 9004, 676.0, 2977.4000000000005, 9003.1, 9004.0, 0.193203310894633, 0.09352989228068034, 0.46621619259827946], "isController": false}, {"data": ["getClassAttendanceSummaries", 55, 3, 5.454545454545454, 512.5090909090909, 7, 9005, 283.0, 867.5999999999995, 1502.9999999999984, 9005.0, 0.18194509227924271, 0.10758731193493644, 0.1936720220550533], "isController": false}, {"data": ["childByID", 56, 3, 5.357142857142857, 5815.160714285712, 42, 17126, 4937.5, 14366.500000000002, 16578.649999999998, 17126.0, 0.18422082813841564, 0.44504184855732065, 2.3452174957152208], "isController": false}, {"data": ["getChildStatementOfAccount", 59, 6, 10.169491525423728, 27075.644067796617, 8464, 51364, 27143.0, 37959.0, 45694.0, 51364.0, 0.20044369400741302, 0.26082297742970034, 0.32669973173669176], "isController": false}, {"data": ["findClassActivities", 58, 8, 13.793103448275861, 16034.551724137928, 7, 23423, 17199.5, 22609.9, 23043.6, 23423.0, 0.19084346229525456, 0.16999887817770817, 0.6254596283817133], "isController": false}, {"data": ["getAllClassInfo", 58, 4, 6.896551724137931, 8318.637931034482, 108, 19585, 7742.5, 16000.500000000002, 18330.05, 19585.0, 0.18943105362858448, 0.5016420845172774, 0.4863420312397936], "isController": false}, {"data": ["getChildrenToAssignToClass", 56, 2, 3.5714285714285716, 9287.589285714286, 1580, 18497, 9030.0, 15791.300000000003, 17407.899999999998, 18497.0, 0.1832658631331263, 0.6106188198905641, 0.7285891882958565], "isController": false}, {"data": ["bankAccountsByFkChild", 57, 1, 1.7543859649122806, 814.017543859649, 8, 11909, 485.0, 1355.4, 1821.3999999999978, 11909.0, 0.2055587411149939, 0.12044809664326182, 0.26357287801170604], "isController": false}, {"data": ["getChildrenData", 55, 1, 1.8181818181818181, 3048.9818181818187, 1460, 9003, 2817.0, 4174.599999999999, 8015.199999999997, 9003.0, 0.18289742779708362, 0.10821539393611893, 0.542976738772592], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 32, 100.0, 6.262230919765166], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 511, 32, "502/Bad Gateway", 32, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["getChildHealthRecords", 57, 4, "502/Bad Gateway", 4, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getClassAttendanceSummaries", 55, 3, "502/Bad Gateway", 3, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["childByID", 56, 3, "502/Bad Gateway", 3, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildStatementOfAccount", 59, 6, "502/Bad Gateway", 6, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["findClassActivities", 58, 8, "502/Bad Gateway", 8, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getAllClassInfo", 58, 4, "502/Bad Gateway", 4, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildrenToAssignToClass", 56, 2, "502/Bad Gateway", 2, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["bankAccountsByFkChild", 57, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getChildrenData", 55, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
